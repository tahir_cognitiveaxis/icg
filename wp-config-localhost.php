<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'icg');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '};LJ)7^aIq?5rh*(=yfM~q{P~b{*OO){&+fUY6 {d_T`{r*m+|A&q1k%*%U}Z#oI');
define('SECURE_AUTH_KEY',  'tpmcJ6I[XD(ke|}etcIS>N8f/TklWEtT8[zM`eMp*)m |mEU1^Jwuq#*W!5%JH)^');
define('LOGGED_IN_KEY',    'Gy{Egv0xPkF~oBYbb?5Rv4Bv:1Yb1jfHh)&mcFOIpf)h=5WI(7SG715^!`{_/%+C');
define('NONCE_KEY',        '`$2W;9>0bDP~6`jTbawY)|2g&%<AiW][Yh?-.A)X-b}NVqNz/H*<y^:-O%~R-rv:');
define('AUTH_SALT',        'w<W<rSCq8JJ4D~Yjt+:Q]?j#^+A)t=Z3l(*z*]h+ep?kTLX`6wZF>D[0h3259~?;');
define('SECURE_AUTH_SALT', 'WrvyC^0) ,}{Qu5LP/K{Nvp^ `XKj)iDXbiMZ9+Op.(S>=o<]CbKz|[Ko&A!tio<');
define('LOGGED_IN_SALT',   'uLY9l{b=WIqrJyY}AI%@U<k#]2b<#TBE8Qzl5TM pa)|V5t:TQx)Z$8gyyYBsE$:');
define('NONCE_SALT',       '^P}d:neoPMcx;W?w%Hn8[ 1k_D-?w&3RSAi{jF@ ?q:eeO!T5KyCd5c/g,>Wr&]t');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'icg_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
